package ru.vartanyan.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.vartanyan.tm.api.service.dto.IProjectService;
import ru.vartanyan.tm.api.service.dto.ITaskService;
import ru.vartanyan.tm.api.service.dto.IUserService;
import ru.vartanyan.tm.api.service.model.IProjectServiceGraph;
import ru.vartanyan.tm.api.service.model.ITaskServiceGraph;
import ru.vartanyan.tm.api.service.model.IUserServiceGraph;
import ru.vartanyan.tm.configuration.ServerConfiguration;
import ru.vartanyan.tm.exception.system.NullObjectException;
import ru.vartanyan.tm.marker.DBCategory;
import ru.vartanyan.tm.model.ProjectGraph;
import ru.vartanyan.tm.model.UserGraph;
import ru.vartanyan.tm.service.TestUtil;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;

public class ProjectServiceGraphTest {

    @NotNull AnnotationConfigApplicationContext context =
            new AnnotationConfigApplicationContext(ServerConfiguration.class);

    @NotNull
    private final IProjectServiceGraph projectService =
            context.getBean(IProjectServiceGraph.class);

    @NotNull
    private final IUserServiceGraph userService =
            context.getBean(IUserServiceGraph.class);

    @NotNull
    private final ITaskServiceGraph taskService =
            context.getBean(ITaskServiceGraph.class);

    {
        TestUtil.initUser();
    }

    @Test
    @Category(DBCategory.class)
    public void addAllTest() {
        final List<ProjectGraph> projects = new ArrayList<>();
        final ProjectGraph project1 = new ProjectGraph();
        final ProjectGraph project2 = new ProjectGraph();
        projects.add(project1);
        projects.add(project2);
        projectService.addAll(projects);
        Assert.assertTrue(projectService.findOneById(project1.getId()) != null);
        Assert.assertTrue(projectService.findOneById(project2.getId()) != null);
        projectService.remove(projects.get(0));
        projectService.remove(projects.get(1));
    }

    @Test
    @Category(DBCategory.class)
    public void addTest() throws NullObjectException {
        final ProjectGraph project = new ProjectGraph();
        projectService.add(project);
        Assert.assertNotNull(projectService.findOneById(project.getId()));
        projectService.remove(project);
    }

    @Test
    @Category(DBCategory.class)
    public void clearTest() {
        taskService.clear();
        projectService.clear();
        Assert.assertTrue(projectService.findAll().isEmpty());
    }

    @Test
    @Category(DBCategory.class)
    public void findAll() {
        final int projectSize = projectService.findAll().size();
        final List<ProjectGraph> projects = new ArrayList<>();
        final ProjectGraph project1 = new ProjectGraph();
        final ProjectGraph project2 = new ProjectGraph();
        projects.add(project1);
        projects.add(project2);
        projectService.addAll(projects);
        Assert.assertEquals(2 + projectSize, projectService.findAll().size());
        projectService.remove(project1);
        projectService.remove(project2);
    }

    @Test
    @Category(DBCategory.class)
    public void findOneByIdTest() throws NullObjectException {
        final ProjectGraph project = new ProjectGraph();
        final String projectId = project.getId();
        projectService.add(project);
        Assert.assertNotNull(projectService.findOneById(projectId));
        projectService.remove(project);
    }

    @Test
    @Category(DBCategory.class)
    public void findOneByIndexTest() throws NullObjectException {
        final ProjectGraph project = new ProjectGraph();
        projectService.add(project);
        final String projectId = project.getId();
        Assert.assertTrue(projectService.findOneById(projectId) != null);
        projectService.remove(project);
    }

    @Test
    @Category(DBCategory.class)
    public void findOneByIndexTestByUserId() throws NullObjectException {
        final ProjectGraph project = new ProjectGraph();
        final @NotNull UserGraph userGraph = userService.findByLogin("test");
        final String userId = userGraph.getId();
        project.setUser(userGraph);
        projectService.add(project);
        final String projectId = project.getId();
        Assert.assertTrue(projectService.findOneById(userId, projectId) != null);
        projectService.remove(project);
    }

    @Test
    @Category(DBCategory.class)
    public void findOneByNameTest() throws NullObjectException {
        final ProjectGraph project = new ProjectGraph();
        final @NotNull UserGraph userGraph = userService.findByLogin("test");
        final String userId = userGraph.getId();
        project.setUser(userGraph);
        project.setName("pr1");
        projectService.add(project);
        final String name = project.getName();
        Assert.assertNotNull(name);
        Assert.assertTrue(projectService.findOneByName(userId, name) != null);
        projectService.remove(project);
    }

    @Test
    @Category(DBCategory.class)
    public void removeOneByIdTest() throws NullObjectException {
        final ProjectGraph project = new ProjectGraph();
        projectService.add(project);
        final String projectId = project.getId();
        projectService.removeOneById(projectId);
        Assert.assertFalse(projectService.findOneById(projectId) != null);
    }


    @Test
    @Category(DBCategory.class)
    public void removeOneByIdTestByUserId() throws NullObjectException {
        final ProjectGraph project = new ProjectGraph();
        final @NotNull UserGraph userGraph = userService.findByLogin("test");
        final String userId = userGraph.getId();
        project.setUser(userGraph);
        projectService.add(project);
        final String projectId = project.getId();
        projectService.removeOneById(userId, projectId);
        Assert.assertFalse(projectService.findOneById(userId, projectId) != null);
    }

    @Test
    @Category(DBCategory.class)
    public void removeOneByIndexTest() throws NullObjectException {
        final ProjectGraph project1 = new ProjectGraph();
        final ProjectGraph project2 = new ProjectGraph();
        final @NotNull UserGraph userGraph = userService.findByLogin("test");
        final String userId = userGraph.getId();
        project1.setUser(userGraph);
        project2.setUser(userGraph);
        projectService.add(project1);
        projectService.add(project2);
        Assert.assertTrue(projectService.findOneByIndex(userId, 1) != null);
        Assert.assertTrue(projectService.findOneByIndex(userId, 2) != null);
    }

    @Test
    @Category(DBCategory.class)
    public void removeOneByNameTest() throws NullObjectException {
        final ProjectGraph project = new ProjectGraph();
        final @NotNull UserGraph userGraph = userService.findByLogin("test");
        final String userId = userGraph.getId();
        project.setUser(userGraph);
        project.setName("pr1");
        projectService.add(project);
        final String name = project.getName();
        Assert.assertNotNull(name);
        projectService.removeOneByName(userId, name);
        Assert.assertFalse(projectService.findOneByName(userId, name) != null);
    }

    @Test
    @Category(DBCategory.class)
    public void removeTest() throws NullObjectException {
        final ProjectGraph project = new ProjectGraph();
        projectService.add(project);
        projectService.remove(project);
        Assert.assertNull(projectService.findOneById(project.getId()));
    }

    @Test
    @Category(DBCategory.class)
    public void removeTestByUserIdAndObject() throws NullObjectException {
        final ProjectGraph project = new ProjectGraph();
        final @NotNull UserGraph userGraph = userService.findByLogin("test");
        final String userId = userGraph.getId();
        project.setUser(userGraph);
        projectService.add(project);
        projectService.remove(userId, project);
        Assert.assertTrue(projectService.findOneById(userId, project.getId()) == null);
    }

}

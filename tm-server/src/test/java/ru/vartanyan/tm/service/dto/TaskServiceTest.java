package ru.vartanyan.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.vartanyan.tm.api.service.dto.*;
import ru.vartanyan.tm.configuration.ServerConfiguration;
import ru.vartanyan.tm.dto.Task;
import ru.vartanyan.tm.dto.User;
import ru.vartanyan.tm.marker.DBCategory;
import ru.vartanyan.tm.service.TestUtil;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;

public class TaskServiceTest {

    @NotNull AnnotationConfigApplicationContext context =
            new AnnotationConfigApplicationContext(ServerConfiguration.class);

    @NotNull
    private final IProjectService projectService = context.getBean(IProjectService.class);

    @NotNull
    private final IUserService userService = context.getBean(IUserService.class);

    @NotNull
    private final ITaskService taskService = context.getBean(ITaskService.class);

    {
        TestUtil.initUser();
    }

    @Test
    @Category(DBCategory.class)
    public void addAllTest() {
        final List<Task> tasks = new ArrayList<>();
        final Task task1 = new Task();
        final Task task2 = new Task();
        tasks.add(task1);
        tasks.add(task2);
        taskService.addAll(tasks);
        Assert.assertTrue(taskService.findOneById(task1.getId()) != null);
        Assert.assertTrue(taskService.findOneById(task2.getId()) != null);
        taskService.remove(tasks.get(0));
        taskService.remove(tasks.get(1));
    }

    @Test
    @Category(DBCategory.class)
    public void addTest() {
        final Task task = new Task();
        taskService.add(task);
        Assert.assertNotNull(taskService.findOneById(task.getId()));
        taskService.remove(task);
    }

    @Test
    @Category(DBCategory.class)
    public void clearTest() {
        taskService.clear();
        Assert.assertTrue(taskService.findAll().isEmpty());
    }

    @Test
    @Category(DBCategory.class)
    public void findAll() {
        final int taskSize = taskService.findAll().size();
        final List<Task> tasks = new ArrayList<>();
        final Task task1 = new Task();
        final Task task2 = new Task();
        tasks.add(task1);
        tasks.add(task2);
        taskService.addAll(tasks);
        Assert.assertEquals(2 + taskSize, taskService.findAll().size());
        taskService.remove(task1);
        taskService.remove(task2);
    }

    @Test
    @Category(DBCategory.class)
    public void findOneByIdTest() {
        final Task task = new Task();
        final String taskId = task.getId();
        taskService.add(task);
        Assert.assertNotNull(taskService.findOneById(taskId));
        taskService.remove(task);
    }

    @Test
    @Category(DBCategory.class)
    public void findOneByIndexTest() {
        final Task task = new Task();
        taskService.add(task);
        final String taskId = task.getId();
        Assert.assertTrue(taskService.findOneById(taskId) != null);
        taskService.remove(task);
    }

    @Test
    @Category(DBCategory.class)
    public void findOneByIndexTestByUserId() {
        final Task task = new Task();
        final @NotNull User user = userService.findByLogin("test");
        final String userId = user.getId();
        task.setUserId(userId);
        taskService.add(task);
        final String taskId = task.getId();
        Assert.assertTrue(taskService.findOneById(userId, taskId) != null);
        taskService.remove(task);
    }

    @Test
    @Category(DBCategory.class)
    public void findOneByNameTest() {
        final Task task = new Task();
        final @Nullable User user = userService.findByLogin("test");
        final String userId = user.getId();
        task.setUserId(userId);
        task.setName("pr1");
        taskService.add(task);
        final String name = task.getName();
        Assert.assertNotNull(name);
        Assert.assertTrue(taskService.findOneByName(userId, name) != null);
        taskService.remove(task);
    }

    @Test
    @Category(DBCategory.class)
    public void removeOneByIdTest() {
        final Task task = new Task();
        taskService.add(task);
        final String taskId = task.getId();
        taskService.removeOneById(taskId);
        Assert.assertFalse(taskService.findOneById(taskId) != null);
    }

    @Test
    @Category(DBCategory.class)
    public void removeOneByIdTestByUserId() {
        final Task task = new Task();
        final @NotNull User user = userService.findByLogin("test");
        final String userId = user.getId();
        task.setUserId(userId);
        taskService.add(task);
        final String taskId = task.getId();
        taskService.removeOneById(userId, taskId);
        Assert.assertFalse(taskService.findOneById(taskId) != null);
    }

    @Test
    @Category(DBCategory.class)
    public void removeOneByIndexTest() {
        final Task task1 = new Task();
        final Task task2 = new Task();
        final Task task3 = new Task();
        final @NotNull User user = userService.findByLogin("test");
        final String userId = user.getId();
        task1.setUserId(userId);
        task2.setUserId(userId);
        task3.setUserId(userId);
        taskService.add(task1);
        taskService.add(task2);
        taskService.add(task3);
        Assert.assertTrue(taskService.findOneByIndex(userId, 1) != null);
        Assert.assertTrue(taskService.findOneByIndex(userId, 2) != null);
        Assert.assertTrue(taskService.findOneByIndex(userId, 3) != null);
    }

    @Test
    @Category(DBCategory.class)
    public void removeOneByNameTest() {
        final Task task = new Task();
        final @NotNull User user = userService.findByLogin("test");
        final String userId = user.getId();
        task.setUserId(userId);
        task.setName("pr1");
        taskService.add(task);
        final String name = task.getName();
        Assert.assertNotNull(name);
        taskService.removeOneByName(userId, name);
        Assert.assertFalse(taskService.findOneByName(userId, name) != null);
    }

    @Test
    @Category(DBCategory.class)
    public void removeTest() {
        final Task task = new Task();
        taskService.add(task);
        taskService.remove(task);
        Assert.assertNull(taskService.findOneById(task.getId()));
    }

    @Test
    @Category(DBCategory.class)
    public void removeTestByUserIdAndObject() {
        final Task task = new Task();
        final @NotNull User user = userService.findByLogin("test");
        final String userId = user.getId();
        task.setUserId(userId);
        taskService.add(task);
        taskService.remove(userId, task);
        Assert.assertFalse(taskService.findOneById(task.getId()) != null);
    }

}

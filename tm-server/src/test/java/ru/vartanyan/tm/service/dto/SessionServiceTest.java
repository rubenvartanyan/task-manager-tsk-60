package ru.vartanyan.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.vartanyan.tm.api.service.dto.*;
import ru.vartanyan.tm.configuration.ServerConfiguration;
import ru.vartanyan.tm.dto.Session;
import ru.vartanyan.tm.marker.DBCategory;
import ru.vartanyan.tm.service.TestUtil;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;

public class SessionServiceTest {

    @NotNull AnnotationConfigApplicationContext context =
            new AnnotationConfigApplicationContext(ServerConfiguration.class);

    @NotNull
    private final IProjectService projectService = context.getBean(IProjectService.class);

    @NotNull
    private final IUserService userService = context.getBean(IUserService.class);

    @NotNull
    private final ITaskService taskService = context.getBean(ITaskService.class);

    @NotNull
    private final ISessionService sessionService = context.getBean(ISessionService.class);

    {
        TestUtil.initUser();
    }

    @Test
    @Category(DBCategory.class)
    public void addAllTest() {
        final List<Session> sessions = new ArrayList<>();
        final Session session1 = new Session();
        final Session session2 = new Session();
        sessions.add(session1);
        sessions.add(session2);
        sessionService.addAll(sessions);
        Assert.assertTrue( sessionService.findOneById(session1.getId()) != null);
        Assert.assertTrue(sessionService.findOneById(session2.getId()) != null);
    }

    @Test
    @Category(DBCategory.class)
    public void addTest() {
        final Session session = new Session();
        sessionService.add(session);
        Assert.assertNotNull(sessionService.findOneById(session.getId()));
    }

    @Test
    @Category(DBCategory.class)
    public void clearTest() {
        sessionService.clear();
        Assert.assertTrue(sessionService.findAll().isEmpty());
    }

    @Test
    @Category(DBCategory.class)
    public void findAll() {
        sessionService.clear();
        final List<Session> sessions = new ArrayList<>();
        final Session session1 = new Session();
        final Session session2 = new Session();
        sessions.add(session1);
        sessions.add(session2);
        sessionService.addAll(sessions);
        Assert.assertEquals(2, sessionService.findAll().size());
    }

    @Test
    @Category(DBCategory.class)
    public void findOneByIdTest() {
        final Session session1 = new Session();
        final String sessionId = session1.getId();
        sessionService.add(session1);
        Assert.assertNotNull(sessionService.findOneById(sessionId));
    }

    @Test
    @Category(DBCategory.class)
    public void findOneByIndexTest() {
        final Session session = new Session();
        sessionService.add(session);
        final String sessionId = session.getId();
        Assert.assertTrue(sessionService.findOneById(sessionId) != null);
    }

    @Test
    @Category(DBCategory.class)
    public void removeOneByIdTest() {
        final Session session = new Session();
        sessionService.add(session);
        final String sessionId = session.getId();
        sessionService.removeOneById(sessionId);
        Assert.assertFalse(sessionService.findOneById(sessionId) != null);
    }

    @Test
    @Category(DBCategory.class)
    public void removeTest() {
        final Session session = new Session();
        sessionService.add(session);
        sessionService.remove(session);
        Assert.assertNull(sessionService.findOneById(session.getId()));
    }

}

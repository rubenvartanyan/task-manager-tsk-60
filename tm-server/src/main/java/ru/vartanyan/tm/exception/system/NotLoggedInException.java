package ru.vartanyan.tm.exception.system;

public class NotLoggedInException extends Exception{

    public NotLoggedInException() {
        super("Error! You have to login to do this operation...");
    }

}

package ru.vartanyan.tm.service.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.vartanyan.tm.api.IPropertyService;
import ru.vartanyan.tm.api.repository.model.IProjectRepositoryGraph;
import ru.vartanyan.tm.api.repository.model.ISessionRepositoryGraph;
import ru.vartanyan.tm.api.repository.model.ITaskRepositoryGraph;
import ru.vartanyan.tm.api.repository.model.IUserRepositoryGraph;
import ru.vartanyan.tm.api.service.IConnectionService;
import ru.vartanyan.tm.api.service.model.ITaskServiceGraph;
import ru.vartanyan.tm.api.service.model.IUserServiceGraph;
import ru.vartanyan.tm.enumerated.Status;
import ru.vartanyan.tm.exception.empty.EmptyDescriptionException;
import ru.vartanyan.tm.exception.empty.EmptyIdException;
import ru.vartanyan.tm.exception.empty.EmptyLoginException;
import ru.vartanyan.tm.exception.empty.EmptyNameException;
import ru.vartanyan.tm.exception.incorrect.IncorrectIndexException;
import ru.vartanyan.tm.exception.system.NullObjectException;
import ru.vartanyan.tm.model.TaskGraph;
import ru.vartanyan.tm.repository.model.TaskRepositoryGraph;
import ru.vartanyan.tm.repository.model.UserRepositoryGraph;

import javax.persistence.EntityManager;
import java.util.List;

@Service
public final class TaskServiceGraph extends AbstractServiceGraph<TaskGraph>
        implements ITaskServiceGraph {

    @NotNull
    @Autowired
    public IUserRepositoryGraph userRepositoryGraph;

    @NotNull
    @Autowired
    public ITaskRepositoryGraph taskRepositoryGraph;

    @NotNull
    public IUserRepositoryGraph getUserRepositoryGraph() {
        return userRepositoryGraph;
    }

    @NotNull
    public ITaskRepositoryGraph getTaskRepositoryGraph() {
        return taskRepositoryGraph;
    }

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @SneakyThrows
    @Override
    @Transactional
    public @NotNull TaskGraph add(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null) throw new EmptyIdException();
        if (name == null) throw new EmptyNameException();
        if (description == null) throw new EmptyDescriptionException();
        @NotNull final TaskGraph task = new TaskGraph();
        task.setName(name);
        task.setDescription(description);
        ITaskRepositoryGraph taskRepositoryGraph = getTaskRepositoryGraph();
        IUserRepositoryGraph userRepositoryGraph = getUserRepositoryGraph();
        task.setUser(userRepositoryGraph.findOneById(userId));
        taskRepositoryGraph.add(task);
        return task;
    }

    @Override
    @SneakyThrows
    @Transactional
    public void add(@Nullable final TaskGraph task) {
        if (task == null) throw new NullObjectException();
        ITaskRepositoryGraph taskRepositoryGraph = getTaskRepositoryGraph();
        taskRepositoryGraph.add(task);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void addAll(@Nullable List<TaskGraph> entities) {
        if (entities == null) throw new NullObjectException();
        ITaskRepositoryGraph taskRepositoryGraph = getTaskRepositoryGraph();
        entities.forEach(taskRepositoryGraph::add);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void clear() {
        ITaskRepositoryGraph taskRepositoryGraph = getTaskRepositoryGraph();
        taskRepositoryGraph.clear();
    }

    @Override
    @SneakyThrows
    @Transactional
    public void remove(final @Nullable TaskGraph entity) {
        if (entity == null) throw new NullObjectException();
        ITaskRepositoryGraph taskRepositoryGraph = getTaskRepositoryGraph();
        taskRepositoryGraph.removeOneById(entity.getId());
    }

    @Override
    @SneakyThrows
    @Transactional
    public @Nullable List<TaskGraph> findAll() {
        ITaskRepositoryGraph taskRepositoryGraph = getTaskRepositoryGraph();
        return taskRepositoryGraph.findAll();
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public TaskGraph findOneById(
            @Nullable final String id
    ) {
        ITaskRepositoryGraph taskRepositoryGraph = getTaskRepositoryGraph();
        if (id == null) throw new EmptyIdException();
        return taskRepositoryGraph.findOneById(id);
    }

    @SneakyThrows
    @Override
    @Transactional
    public void removeOneById(
            @Nullable final String id
    ) {
        if (id == null) throw new EmptyLoginException();
        ITaskRepositoryGraph taskRepositoryGraph = getTaskRepositoryGraph();
        taskRepositoryGraph.removeOneById(id);
    }

    @SneakyThrows
    @Override
    @Transactional
    public void clear(@Nullable final String userId) {
        if (userId == null) throw new EmptyIdException();
        ITaskRepositoryGraph taskRepositoryGraph = getTaskRepositoryGraph();
        taskRepositoryGraph.clearByUserId(userId);
    }

    @SneakyThrows
    @Override
    @Transactional
    public @NotNull List<TaskGraph> findAll(@NotNull final String userId) {
        if (userId == null) throw new EmptyIdException();
        ITaskRepositoryGraph taskRepositoryGraph = getTaskRepositoryGraph();
        return taskRepositoryGraph.findAllByUserId(userId);
    }

    @SneakyThrows
    @Override
    @Transactional
    public @NotNull TaskGraph findOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (userId == null) throw new EmptyIdException();
        if (id == null) throw new EmptyIdException();
        ITaskRepositoryGraph taskRepositoryGraph = getTaskRepositoryGraph();
        return taskRepositoryGraph.findOneByIdAndUserId(userId, id);
    }

    @SneakyThrows
    @Override
    @Transactional
    public @Nullable TaskGraph findOneByIndex(
            @Nullable final String userId,
            @Nullable final Integer index
    ) {
        if (userId == null) throw new EmptyIdException();
        if (index == null || index == 0 || index < 0) throw new IncorrectIndexException();
        ITaskRepositoryGraph taskRepositoryGraph = getTaskRepositoryGraph();
        return taskRepositoryGraph.findOneByIndex(userId, index);
    }

    @SneakyThrows
    @Override
    @Transactional
    public @Nullable TaskGraph findOneByName(
            @Nullable final String userId,
            @Nullable final String name
    ) {
        if (userId == null) throw new EmptyIdException();
        if (name == null) throw new EmptyNameException();
        ITaskRepositoryGraph taskRepositoryGraph = getTaskRepositoryGraph();
        return taskRepositoryGraph.findOneByName(userId, name);
    }

    @SneakyThrows
    @Override
    @Transactional
    public void remove(
            @Nullable final String userId,
            @Nullable final TaskGraph entity
    ) {
        if (userId == null) throw new EmptyIdException();
        if (entity == null) throw new NullObjectException();
        ITaskRepositoryGraph taskRepositoryGraph = getTaskRepositoryGraph();
        taskRepositoryGraph.removeOneByIdAndUserId(userId, entity.getId());
    }

    @SneakyThrows
    @Override
    @Transactional
    public void removeOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (userId == null) throw new EmptyIdException();
        if (id == null) throw new EmptyIdException();
        ITaskRepositoryGraph taskRepositoryGraph = getTaskRepositoryGraph();
        taskRepositoryGraph.removeOneByIdAndUserId(userId, id);
    }

    @SneakyThrows
    @Override
    @Transactional
    public void removeOneByIndex(
            @Nullable final String userId,
            @Nullable final Integer index
    ) {
        if (userId == null) throw new EmptyIdException();
        if (index == null || index == 0 || index < 0) throw new IncorrectIndexException();
        ITaskRepositoryGraph taskRepositoryGraph = getTaskRepositoryGraph();
        @Nullable TaskGraph task = taskRepositoryGraph.findOneByIndex(userId, index);
        taskRepositoryGraph.removeOneByIdAndUserId(userId, task.getId());
    }

    @SneakyThrows
    @Override
    @Transactional
    public void removeOneByName(
            @Nullable final String userId,
            @Nullable final String name
    ) {
        if (userId == null) throw new EmptyIdException();
        if (name == null) throw new EmptyNameException();
        ITaskRepositoryGraph taskRepositoryGraph = getTaskRepositoryGraph();
        taskRepositoryGraph.removeOneByName(userId, name);
    }

    @SneakyThrows
    @Override
    @Transactional
    public void changeStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (userId == null) throw new EmptyIdException();
        if (id == null) throw new EmptyIdException();
        if (status == null) throw new NullObjectException();
        @NotNull final TaskGraph entity = findOneById(userId, id);
        entity.setStatus(status);
        ITaskRepositoryGraph taskRepositoryGraph = getTaskRepositoryGraph();
        taskRepositoryGraph.update(entity);
    }

    @SneakyThrows
    @Override
    @Transactional
    public void changeStatusByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final Status status
    ) {
        if (userId == null) throw new EmptyIdException();
        if (index == null || index == 0 || index < 0) throw new IncorrectIndexException();
        if (status == null) throw new NullObjectException();
        @NotNull final TaskGraph entity = findOneByIndex(userId, index);
        entity.setStatus(status);
        ITaskRepositoryGraph taskRepositoryGraph = getTaskRepositoryGraph();
        taskRepositoryGraph.update(entity);
    }

    @SneakyThrows
    @Override
    @Transactional
    public void changeStatusByName(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final Status status
    ) {
        if (userId == null) throw new EmptyIdException();
        if (name == null) throw new EmptyNameException();
        if (status == null) throw new NullObjectException();
        @NotNull final TaskGraph entity = findOneByName(userId, name);
        entity.setStatus(status);
        ITaskRepositoryGraph taskRepositoryGraph = getTaskRepositoryGraph();
        taskRepositoryGraph.update(entity);
    }

    @SneakyThrows
    @Override
    @Transactional
    public void finishById(
            @Nullable final String userId, @Nullable final String id
    ) {
        if (userId == null) throw new EmptyIdException();
        if (id == null) throw new EmptyIdException();
        @NotNull final TaskGraph entity = findOneById(userId, id);
        entity.setStatus(Status.COMPLETE);
        ITaskRepositoryGraph taskRepositoryGraph = getTaskRepositoryGraph();
        taskRepositoryGraph.update(entity);
    }

    @SneakyThrows
    @Override
    @Transactional
    public void finishByIndex(
            @Nullable final String userId, @Nullable final Integer index
    ) {
        if (userId == null) throw new EmptyIdException();
        if (index == null || index == 0 || index < 0) throw new IncorrectIndexException();
        @NotNull final TaskGraph entity = findOneByIndex(userId, index);
        entity.setStatus(Status.COMPLETE);
        ITaskRepositoryGraph taskRepositoryGraph = getTaskRepositoryGraph();
        taskRepositoryGraph.update(entity);
    }

    @SneakyThrows
    @Override
    @Transactional
    public void finishByName(
            @Nullable final String userId, @Nullable final String name
    ) {
        if (userId == null) throw new EmptyIdException();
        if (name == null) throw new EmptyNameException();
        @NotNull final TaskGraph entity = findOneByName(userId, name);
        entity.setStatus(Status.COMPLETE);
        ITaskRepositoryGraph taskRepositoryGraph = getTaskRepositoryGraph();
        taskRepositoryGraph.update(entity);
    }

    @SneakyThrows
    @Override
    @Transactional
    public void startById(
            @Nullable final String userId, @Nullable final String id
    ) {
        if (userId == null) throw new EmptyIdException();
        if (id == null) throw new EmptyIdException();
        @NotNull final TaskGraph entity = findOneById(userId, id);
        entity.setStatus(Status.IN_PROGRESS);
        ITaskRepositoryGraph taskRepositoryGraph = getTaskRepositoryGraph();
        taskRepositoryGraph.update(entity);
    }

    @SneakyThrows
    @Override
    @Transactional
    public void startByIndex(
            @Nullable final String userId, @Nullable final Integer index
    ) {
        if (userId == null) throw new EmptyIdException();
        if (index == null || index == 0 || index < 0) throw new IncorrectIndexException();
        @NotNull final TaskGraph entity = findOneByIndex(userId, index);
        entity.setStatus(Status.IN_PROGRESS);
        ITaskRepositoryGraph taskRepositoryGraph = getTaskRepositoryGraph();
        taskRepositoryGraph.update(entity);
    }

    @SneakyThrows
    @Override
    @Transactional
    public void startByName(
            @Nullable final String userId, @Nullable final String name
    ) {
        if (userId == null) throw new EmptyIdException();
        if (name == null) return;
        @NotNull final TaskGraph entity = findOneByName(userId, name);
        entity.setStatus(Status.IN_PROGRESS);
        ITaskRepositoryGraph taskRepositoryGraph = getTaskRepositoryGraph();
        taskRepositoryGraph.update(entity);
    }

    @SneakyThrows
    @Override
    @Transactional
    public void updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null) throw new EmptyIdException();
        if (id == null) throw new EmptyIdException();
        if (name == null) throw new EmptyNameException();
        @NotNull final TaskGraph entity = findOneById(userId, id);
        entity.setName(name);
        entity.setDescription(description);
        ITaskRepositoryGraph taskRepositoryGraph = getTaskRepositoryGraph();
        taskRepositoryGraph.update(entity);
    }

    @SneakyThrows
    @Override
    @Transactional
    public void updateByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null) throw new EmptyIdException();
        if (index == null || index == 0 || index < 0) throw new IncorrectIndexException();
        if (name == null) throw new EmptyNameException();
        @NotNull final TaskGraph entity = findOneByIndex(userId, index);
        entity.setName(name);
        entity.setDescription(description);
        ITaskRepositoryGraph taskRepositoryGraph = getTaskRepositoryGraph();
        taskRepositoryGraph.update(entity);
    }

}

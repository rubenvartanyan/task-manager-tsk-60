package ru.vartanyan.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import ru.vartanyan.tm.api.IPropertyService;
import ru.vartanyan.tm.api.repository.dto.IProjectRepository;
import ru.vartanyan.tm.api.repository.dto.ISessionRepository;
import ru.vartanyan.tm.api.repository.dto.ITaskRepository;
import ru.vartanyan.tm.api.repository.dto.IUserRepository;
import ru.vartanyan.tm.api.service.IConnectionService;
import ru.vartanyan.tm.api.service.dto.IService;
import ru.vartanyan.tm.dto.AbstractEntity;

import javax.persistence.EntityManager;

@Service
public abstract class AbstractService<E extends AbstractEntity> implements IService<E> {

}

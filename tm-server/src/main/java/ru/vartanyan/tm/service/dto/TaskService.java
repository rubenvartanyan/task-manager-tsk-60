package ru.vartanyan.tm.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.vartanyan.tm.api.IPropertyService;
import ru.vartanyan.tm.api.repository.dto.IProjectRepository;
import ru.vartanyan.tm.api.repository.dto.ISessionRepository;
import ru.vartanyan.tm.api.repository.dto.ITaskRepository;
import ru.vartanyan.tm.api.repository.dto.IUserRepository;
import ru.vartanyan.tm.api.service.IConnectionService;
import ru.vartanyan.tm.api.service.dto.ITaskService;
import ru.vartanyan.tm.api.service.dto.IUserService;
import ru.vartanyan.tm.dto.Task;
import ru.vartanyan.tm.enumerated.Status;
import ru.vartanyan.tm.exception.empty.EmptyDescriptionException;
import ru.vartanyan.tm.exception.empty.EmptyIdException;
import ru.vartanyan.tm.exception.empty.EmptyLoginException;
import ru.vartanyan.tm.exception.empty.EmptyNameException;
import ru.vartanyan.tm.exception.incorrect.IncorrectIndexException;
import ru.vartanyan.tm.exception.system.NullObjectException;
import ru.vartanyan.tm.repository.dto.TaskRepository;

import javax.persistence.EntityManager;
import java.util.List;

@Service
public final class TaskService extends AbstractService<Task>
        implements ITaskService {

    @NotNull
    @Autowired
    public ITaskRepository taskRepository;

    @NotNull
    public ITaskRepository getTaskRepository() {
        return taskRepository;
    }

    @SneakyThrows
    @Override
    @Transactional
    public @NotNull Task add(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null) throw new EmptyIdException();
        if (name == null) throw new EmptyIdException();
        if (description == null) throw new EmptyDescriptionException();
        @NotNull final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        task.setUserId(userId);
        ITaskRepository taskRepository = getTaskRepository();
        taskRepository.add(task);
        return task;
    }

    @Override
    @SneakyThrows
    @Transactional
    public void add(@Nullable final Task task) {
        if (task == null) throw new NullObjectException();
        ITaskRepository taskRepository = getTaskRepository();
        taskRepository.add(task);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void addAll(@Nullable List<Task> entities) {
        if (entities == null) throw new NullObjectException();
        ITaskRepository taskRepository = getTaskRepository();
        entities.forEach(taskRepository::add);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void clear() {
        ITaskRepository taskRepository = getTaskRepository();
        taskRepository.clear();
    }

    @Override
    @SneakyThrows
    @Transactional
    public void remove(@Nullable final Task entity) {
        ITaskRepository taskRepository = getTaskRepository();
        if (entity == null) throw new NullObjectException();
        taskRepository.removeOneById(entity.getId());
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public List<Task> findAll() {
        ITaskRepository taskRepository = getTaskRepository();
        return taskRepository.findAll();
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public Task findOneById(
            @Nullable final String id
    ) {
        if (id == null) throw new EmptyIdException();
        ITaskRepository taskRepository = getTaskRepository();
        return taskRepository.findOneById(id);
    }

    @SneakyThrows
    @Override
    @Transactional
    public void removeOneById(
            @Nullable final String id
    ) {
        if (id == null) throw new EmptyLoginException();
        ITaskRepository taskRepository = getTaskRepository();
        taskRepository.removeOneById(id);
    }

    @SneakyThrows
    @Override
    @Transactional
    public void clear(@Nullable final String userId) {
        if (userId == null) throw new EmptyIdException();
        ITaskRepository taskRepository = getTaskRepository();
        taskRepository.clearByUserId(userId);
    }

    @NotNull
    @SneakyThrows
    @Override
    @Transactional
    public List<Task> findAll(@Nullable final String userId) {
        ITaskRepository taskRepository = getTaskRepository();
        if (userId == null) throw new EmptyIdException();
        return taskRepository.findAllByUserId(userId);
    }

    @SneakyThrows
    @NotNull
    @Override
    @Transactional
    public Task findOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (userId == null) throw new EmptyIdException();
        if (id == null) throw new EmptyIdException();
        ITaskRepository taskRepository = getTaskRepository();
        return taskRepository.findOneByIdAndUserId(userId, id);
    }

    @NotNull
    @SneakyThrows
    @Override
    @Transactional
    public Task findOneByIndex(
            @Nullable final String userId,
            @Nullable final Integer index
    ) {
        if (userId == null) throw new EmptyIdException();
        if (index == null || index == 0 || index < 0) throw new IncorrectIndexException();
        ITaskRepository taskRepository = getTaskRepository();
        return taskRepository.findOneByIndex(userId, index);
    }

    @SneakyThrows
    @Override
    @Transactional
    public @Nullable Task findOneByName(
            @Nullable final String userId,
            @Nullable final String name
    ) {
        if (userId == null) throw new EmptyIdException();
        if (name == null) throw new EmptyNameException();
        ITaskRepository taskRepository = getTaskRepository();
        return taskRepository.findOneByName(userId, name);
    }

    @SneakyThrows
    @Override
    @Transactional
    public void remove(
            @Nullable final String userId, @Nullable final Task entity
    ) {
        if (userId == null) throw new EmptyIdException();
        if (entity == null) throw new NullObjectException();
        ITaskRepository taskRepository = getTaskRepository();
        taskRepository.removeOneByIdAndUserId(userId, entity.getId());
    }

    @SneakyThrows
    @Override
    @Transactional
    public void removeOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (userId == null) throw new EmptyIdException();
        if (id == null) throw new EmptyIdException();
        ITaskRepository taskRepository = getTaskRepository();
        taskRepository.removeOneByIdAndUserId(userId, id);
    }

    @SneakyThrows
    @Override
    @Transactional
    public void removeOneByIndex(
            @Nullable final String userId,
            @Nullable final Integer index
    ) {
        if (userId == null) throw new EmptyIdException();
        if (index == null) throw new IncorrectIndexException();
        ITaskRepository taskRepository = getTaskRepository();
        @NotNull Task task = taskRepository.findOneByIndex(userId, index);
        taskRepository.removeOneByIdAndUserId(userId, task.getId());
    }

    @SneakyThrows
    @Override
    @Transactional
    public void removeOneByName(
            @Nullable final String userId,
            @Nullable final String name
    ) {
        if (userId == null) throw new EmptyIdException();
        if (name == null) throw new EmptyNameException();
        ITaskRepository taskRepository = getTaskRepository();
        taskRepository.removeOneByName(userId, name);
    }

    @SneakyThrows
    @Override
    @Transactional
    public void changeStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (userId == null) throw new EmptyIdException();
        if (id == null) throw new EmptyIdException();
        if (status == null) throw new NullObjectException();
        @NotNull final Task entity = findOneById(userId, id);
        if (entity == null) throw new NullObjectException();
        entity.setStatus(status);
        ITaskRepository taskRepository = getTaskRepository();
        taskRepository.update(entity);
    }

    @SneakyThrows
    @Override
    @Transactional
    public void changeStatusByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final Status status
    ) {
        if (userId == null) throw new EmptyIdException();
        if (index == null || index == 0 || index < 0) throw new IncorrectIndexException();
        if (status == null) throw new NullObjectException();
        @NotNull final Task entity = findOneByIndex(userId, index);
        if (entity == null) throw new NullObjectException();
        entity.setStatus(status);
        ITaskRepository taskRepository = getTaskRepository();
        taskRepository.update(entity);
    }

    @SneakyThrows
    @Override
    @Transactional
    public void changeStatusByName(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final Status status
    ) {
        if (userId == null) throw new EmptyIdException();
        if (name == null) throw new EmptyNameException();
        if (status == null) throw new NullObjectException();
        @NotNull final Task entity = findOneByName(userId, name);
        entity.setStatus(status);
        ITaskRepository taskRepository = getTaskRepository();
        taskRepository.update(entity);
    }

    @SneakyThrows
    @Override
    @Transactional
    public void finishById(
            @Nullable final String userId, 
            @Nullable final String id
    ) {
        if (userId == null) throw new EmptyIdException();
        if (id == null) throw new EmptyIdException();
        @NotNull final Task entity = findOneById(userId, id);
        entity.setStatus(Status.COMPLETE);
        ITaskRepository taskRepository = getTaskRepository();
        taskRepository.update(entity);
    }

    @SneakyThrows
    @Override
    @Transactional
    public void finishByIndex(
            @Nullable final String userId, 
            @Nullable final Integer index
    ) {
        if (userId == null) throw new EmptyIdException();
        if (index == null || index == 0 || index < 0) throw new IncorrectIndexException();
        @NotNull final Task entity = findOneByIndex(userId, index);
        if (entity == null) throw new NullObjectException();
        entity.setStatus(Status.COMPLETE);
        ITaskRepository taskRepository = getTaskRepository();
        taskRepository.update(entity);
    }

    @SneakyThrows
    @Override
    @Transactional
    public void finishByName(
            @Nullable final String userId, @Nullable final String name
    ) {
        if (userId == null) throw new EmptyIdException();
        if (name == null) throw new EmptyIdException();
        @NotNull final Task entity = findOneByName(userId, name);
        if (entity == null) throw new NullObjectException();
        entity.setStatus(Status.COMPLETE);
        ITaskRepository taskRepository = getTaskRepository();
        taskRepository.update(entity);
    }

    @SneakyThrows
    @Override
    @Transactional
    public void startById(
            @Nullable final String userId, @Nullable final String id
    ) {
        if (userId == null) throw new EmptyIdException();
        if (id == null) throw new EmptyIdException();
        @NotNull final Task entity = findOneById(userId, id);
        if (entity == null) throw new NullObjectException();
        entity.setStatus(Status.IN_PROGRESS);
        ITaskRepository taskRepository = getTaskRepository();
        taskRepository.update(entity);
    }

    @SneakyThrows
    @Override
    @Transactional
    public void startByIndex(
            @Nullable final String userId,
            @Nullable final Integer index
    ) {
        if (userId == null) throw new EmptyIdException();
        if (index == null || index == 0 || index < 0) throw new IncorrectIndexException();
        @NotNull final Task entity = findOneByIndex(userId, index);
        if (entity == null) throw new NullObjectException();
        entity.setStatus(Status.IN_PROGRESS);
        ITaskRepository taskRepository = getTaskRepository();
        taskRepository.update(entity);
    }

    @SneakyThrows
    @Override
    @Transactional
    public void startByName(
            @Nullable final String userId,
            @Nullable final String name
    ) {
        if (userId == null) throw new EmptyIdException();
        if (name == null) return;
        @NotNull final Task entity = findOneByName(userId, name);
        if (entity == null) throw new NullObjectException();
        entity.setStatus(Status.IN_PROGRESS);
        ITaskRepository taskRepository = getTaskRepository();
        taskRepository.update(entity);
    }

    @SneakyThrows
    @Override
    @Transactional
    public void updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null) throw new EmptyIdException();
        if (id == null) throw new EmptyIdException();
        if (name == null) throw new EmptyNameException();
        @NotNull final Task entity = findOneById(userId, id);
        if (entity == null) throw new NullObjectException();
        entity.setName(name);
        entity.setDescription(description);
        ITaskRepository taskRepository = getTaskRepository();
        taskRepository.update(entity);
    }

    @SneakyThrows
    @Override
    @Transactional
    public void updateByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null) throw new EmptyNameException();
        if (index == null || index == 0 || index < 0) throw new IncorrectIndexException();
        if (name == null) throw new EmptyNameException();
        @NotNull final Task entity = findOneByIndex(userId, index);
        if (entity == null) throw new NullObjectException();
        entity.setName(name);
        entity.setDescription(description);
        ITaskRepository taskRepository = getTaskRepository();
        taskRepository.update(entity);
    }
}

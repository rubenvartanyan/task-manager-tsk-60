package ru.vartanyan.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.vartanyan.tm.api.endpoint.EndpointLocator;
import ru.vartanyan.tm.bootstrap.Bootstrap;
import ru.vartanyan.tm.configuration.ClientConfiguration;
import ru.vartanyan.tm.marker.IntegrationCategory;


import java.util.List;

public class TaskEndpointTest {

    @NotNull
    public final AnnotationConfigApplicationContext context =
            new AnnotationConfigApplicationContext(ClientConfiguration.class);

    @NotNull
    private final SessionEndpoint sessionEndpoint = context.getBean(SessionEndpoint.class);

    @NotNull
    private final TaskEndpoint taskEndpoint = context.getBean(TaskEndpoint.class);

    @NotNull
    private final ProjectEndpoint projectEndpoint = context.getBean(ProjectEndpoint.class);

    private SessionDTO session;

    @Before
    @SneakyThrows
    public void before() {
        session = sessionEndpoint.openSession("test", "test");
        taskEndpoint.clearTasks(session);
    }

    @After
    @SneakyThrows
    public void after() {
        sessionEndpoint.closeSession(session);
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void addTaskTest() {
        final String taskName = "nameTest";
        final String taskDescription = "nameTest";
        taskEndpoint.addTask(taskName, taskDescription, session);
        final TaskDTO task = taskEndpoint.findTaskByName(taskName, session);
        Assert.assertNotNull(task);
        Assert.assertEquals(taskName, task.getDescription());
        Assert.assertEquals(taskDescription, task.getDescription());
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void findAllTaskTest() {
        taskEndpoint.addTask("task1", "description1", session);
        taskEndpoint.addTask("task2", "description2", session);
        taskEndpoint.addTask("task3", "description3", session);
        Assert.assertEquals(3, taskEndpoint.findAllTasks(session).size());
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void findTaskByIdTest() {
        Assert.assertEquals(0, taskEndpoint.findAllTasks(session).size());
        taskEndpoint.addTask("AA", "BB", session);
        List<TaskDTO> taskList = taskEndpoint.findAllTasks(session);
        final TaskDTO task = taskList.get(0);
        final TaskDTO taskFound = taskEndpoint.findTaskById(task.id, session);
        Assert.assertEquals("AA", taskFound.name);
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void findTaskOneByIndexTest() {
        Assert.assertEquals(0, taskEndpoint.findAllTasks(session).size());
        taskEndpoint.addTask("AA", "BB", session);
        final TaskDTO taskFound = taskEndpoint.findTaskByIndex(0, session);
        Assert.assertEquals("AA", taskFound.getName());
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void findTaskOneByNameTest() {
        Assert.assertEquals(0, taskEndpoint.findAllTasks(session).size());
        taskEndpoint.addTask("AA", "BB", session);
        final TaskDTO taskFound = taskEndpoint.findTaskByName("AA", session);
        Assert.assertEquals("AA", taskFound.getName());
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void removeTaskOneByIdTest() {
        Assert.assertEquals(0, taskEndpoint.findAllTasks(session).size());
        taskEndpoint.addTask("AA", "BB", session);
        List<TaskDTO> taskList = taskEndpoint.findAllTasks(session);
        final TaskDTO task = taskList.get(0);
        taskEndpoint.removeTaskById(task.getId(), session);
        Assert.assertEquals(0, taskEndpoint.findAllTasks(session).size());
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void removeTaskByNameTest() {
        Assert.assertEquals(0, taskEndpoint.findAllTasks(session).size());
        taskEndpoint.addTask("AA", "BB", session);
        taskEndpoint.removeTaskByName("AA", session);
        Assert.assertEquals(0, taskEndpoint.findAllTasks(session).size());
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void removeTaskByIndexTest() {
        Assert.assertEquals(0, taskEndpoint.findAllTasks(session).size());
        taskEndpoint.addTask("AA", "BB", session);
        taskEndpoint.removeTaskByIndex(0, session);
        Assert.assertEquals(0, taskEndpoint.findAllTasks(session).size());
    }

}

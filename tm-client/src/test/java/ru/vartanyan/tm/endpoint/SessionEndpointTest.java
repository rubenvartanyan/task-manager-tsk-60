package ru.vartanyan.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.vartanyan.tm.api.endpoint.EndpointLocator;
import ru.vartanyan.tm.bootstrap.Bootstrap;
import ru.vartanyan.tm.configuration.ClientConfiguration;
import ru.vartanyan.tm.exception.system.NotLoggedInException;
import ru.vartanyan.tm.marker.IntegrationCategory;

import java.util.List;

public class SessionEndpointTest {

    @NotNull
    public final AnnotationConfigApplicationContext context =
            new AnnotationConfigApplicationContext(ClientConfiguration.class);

    @NotNull
    private final SessionEndpoint sessionEndpoint = context.getBean(SessionEndpoint.class);

    @NotNull
    private final TaskEndpoint taskEndpoint = context.getBean(TaskEndpoint.class);
    
    @Before
    @SneakyThrows
    public void before() {
    }
    
    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void openSessionTest() {
        final SessionDTO session = sessionEndpoint.openSession("test", "test");
        Assert.assertNotNull(session.getUserId());
        sessionEndpoint.closeSession(session);
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void closeSessionTest() {
        final SessionDTO session = sessionEndpoint.openSession("test", "test");
        sessionEndpoint.closeSession(session);
    }


}
